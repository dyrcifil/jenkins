package cviceni;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Ukol2Test {

	@Test(dataProvider = "numbers")
	public void testKrat(int a, int b, int expected) {
		Assert.assertEquals(Ukol2.krat(a, b), expected);
	}

	@DataProvider(name = "numbers")
	public Object[][] provideNames() {
		return new Object[][] { 
				{ 2, 3, 6 },
				{ -1, -1, 1 },
				{ 1000000000, 1, 1000000000 }
				};

	}
}
